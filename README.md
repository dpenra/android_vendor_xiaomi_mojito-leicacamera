## Leica Camera Configuration for Redmi Note 10

### How to?

1. Add this line to your BoardConfig.mk
```
# Inherit from proprietary files for Leica Camera
include vendor/xiaomi/mojito-leicacamera/BoardConfigVendor.mk
```
2. And this line to your device.mk
```
# Call the Leica Camera setup
$(call inherit-product-if-exists, vendor/xiaomi/mojito-leicacamera/mojito-leicacamera-vendor.mk)
```
3. Clone this repository to vendor/xiaomi/mojito-leicacamera, preferably with `--depth=1` to avoid having a big size locally clone
```sh
git clone --depth=1 https://gitlab.com/dpenra/android_vendor_xiaomi_mojito-leicacamera -b main vendor/xiaomi/mojito-leicacamera
```
4. If you want to include Miui Gallery on your build, set environment variable below
```sh
export TARGET_USE_MIUI_GALLERY=true
```
and/or if you want to include Miui Scanner, set environment variable below
```sh
export TARGET_USE_MIUI_SCANNER=true
```
5. Start building and enjoy!


